<p center="center">
    <h1>病歷整合查詢(護理)</h1>
</p>

<p align="center">
    <a href="https://vuejs.org/v2/guide/">
        <img src="https://img.shields.io/badge/vue-4.5.14-blue">
    </a>
    <a href="https://getbootstrap.com/docs/4.6/getting-started/introduction/">
        <img src="https://img.shields.io/badge/bootstrap-4.5.2-blue">
    </a>
</p>

## 簡介
病歷整合查詢是給護理部一套後台病歷查詢方案

## 功能

```textile
入院護理評估(包含成人、兒科、婦科、產科、新生兒、身心科)之病歷查詢
每日身體評估之病歷查詢
護理指導評估之病歷查詢
護理約束登入之病歷查詢
```

### 開發
```
#克隆項目
git clone https://gitlab.com/whiteHatMimic/nurw200.git

#進入目錄
cd nurw200

#安裝依賴
npm install

#啟動服務
npm serve
```

## 發佈
```git
#打包檔案
npm build
```