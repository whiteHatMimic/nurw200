const state = {
    loading: false //儲存是某啟用 loading 特效， true 為啟用特效， false 為關閉特效
}

const mutations = {
    SET_LOADING: (state, loading) => { //變更 isLoading 狀態
        state.loading = loading
    }
}

const actions = {
    setLoading: ({commit}, loading) => { //呼叫 mutations 變更 isLoading 狀態
        commit('SET_LOADING', loading);
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}